import json
import os

import flask
from flask_restful import Resource, Api, abort, reqparse
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_, not_
from sqlalchemy.dialects.sqlite import JSON
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.sql.expression import alias, exists


class AlchemyEncoder(json.JSONEncoder):
    # JSON encoder for SQLAlchemy objects
    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            return obj.as_dict()

        return super(AlchemyEncoder, self).default(obj)


app = flask.Flask(__name__)

# SQLite config (use an in-memory object by default)
DB_URL = os.environ.get("SQLITE_DB", ":memory:")
app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{DB_URL}"
db = SQLAlchemy(app)

# RESTFUL API config
api = Api(app)
app.config["RESTFUL_JSON"] = {
    "separators": (", ", ": "),
    "indent": 2,
    "cls": AlchemyEncoder
}

# Arguments definition for the api
parser = reqparse.RequestParser()
parser.add_argument("title", type=str)
parser.add_argument("props", type=dict)
parser.add_argument("children", type=list, location="json", required=False)

# Intermediate table for self-referencing many-to-many children field on `Part` model
parts = db.Table(
    "parts",
    db.Column("parent_id", db.Integer, db.ForeignKey("part.id"), primary_key=True),
    db.Column("child_id", db.Integer, db.ForeignKey("part.id"), primary_key=True)
)


class Part(db.Model):
    """
    Part ORM
    """
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    props = db.Column(JSON)
    children = db.relationship(
        "Part",
        secondary=parts,
        primaryjoin=id == parts.columns["parent_id"],
        secondaryjoin=id == parts.columns["child_id"],
        lazy="subquery",
        backref=db.backref("parents", lazy=True)
    )

    def __repr__(self):
        return f"{self.title}"

    def as_dict(self):
        data = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        data["children"] = [c.id for c in self.children]
        return data

    @classmethod
    def get_assemblies(cls):
        """
        The inner query is equivalent to:
        SELECT parent_id id FROM parts parent_parts
        """
        return cls.query.join(db.select([parts.columns.parent_id])).all()

    @classmethod
    def get_top_assemblies(cls):
        """
        The inner query is equivalent to:
        SELECT parent_id id FROM parts parent_parts
        WHERE EXIST(SELECT child_id FROM child_parts WHERE parent_parts.parent_id = child_parts.child_id)
        """
        parent_table = alias(parts, "parent_table")
        child_table = alias(parts, "child_table")
        return cls.query.join(
            db.select(
                [parent_table.columns.parent_id]
            ).where(
                not_(
                    exists(
                        db.select(
                            [child_table.columns.child_id]
                        ).where(
                            parent_table.columns.parent_id == child_table.columns.child_id
                        )
                    )
                )
            )
        ).all()

    @classmethod
    def get_sub_assemblies(cls):
        """
        The inner query is equivalent to:
        SELECT parent_id id FROM parts parent_parts
        WHERE EXIST(SELECT child_id FROM child_parts WHERE parent_parts.parent_id = child_parts.child_id)
        """
        parent_table = alias(parts, "parent_table")
        child_table = alias(parts, "child_table")
        return cls.query.join(
            db.select(
                [parent_table.columns.parent_id]
            ).where(
                exists(
                    db.select(
                        [child_table.columns.child_id]
                    ).where(
                        parent_table.columns.parent_id == child_table.columns.child_id
                    )
                )
            )
        ).all()

    @classmethod
    def get_components(cls):
        parent_table = alias(parts, "parent_table")
        child_table = alias(parts, "child_table")
        return cls.query.join(
            db.select(
                [child_table.columns.child_id]
            ).where(
                not_(
                    exists(
                        db.select(
                            [parent_table.columns.parent_id]
                        ).where(
                            parent_table.columns.parent_id == child_table.columns.child_id
                        )
                    )
                )
            )
        ).all()

    @classmethod
    def get_orphans(cls):
        return cls.query.filter(
            and_(
                ~cls.id.in_(db.select([parts.columns.parent_id])),
                ~cls.id.in_(db.select([parts.columns.child_id]))
            )
        ).all()

    @classmethod
    def get_parents(cls, id):
        """
        The inner query is equivalent to:
        WITH RECURSIVE foo_1 AS
        (SELECT parts.parent_id AS parent_id, parts.child_id AS child_id
        FROM parts
        WHERE parts.child_id = <id>
        UNION ALL SELECT parts_1.parent_id AS parts_1_parent_id, parts_1.child_id AS parts_1_child_id
        FROM parts AS parts_1 JOIN foo_1 AS foo_2 ON parts_1.child_id = anon_2.parent_id)
        """
        cte = db.session.query(
            parts.columns.parent_id, parts.columns.child_id
        ).filter(
            parts.columns.child_id == id
        ).cte(
            recursive=True
        )
        parents = db.aliased(cte)
        parents_query = cte.union_all(db.session.query(parts).join(parents, parts.c.child_id == parents.c.parent_id))
        return cls.query.join(
            parents_query,
            parents_query.columns.parent_id == cls.id
        ).all()


class BaseResource(Resource):
    def get_args(self):
        args = parser.parse_args()
        return dict(((key, args[key]) for key in args if key in self.ARGS))

    def create(self, obj):
        db.session.add(obj)
        db.session.commit()

    def update(self, obj):
        db.session.commit()

    def remove(self, obj):
        db.session.delete(obj)
        db.session.commit()


class PartListResource(BaseResource):
    """
    API resource for part list and create
    """

    ARGS = ("title", "props")

    def get(self):
        # List all parts
        return Part.query.all()

    def post(self):
        # Create a new part
        part = Part(**self.get_args())
        self.create(part)
        return part


api.add_resource(PartListResource, "/parts/")


class PartResource(BaseResource):
    """
    API resource for part details, update, and delete
    """
    ARGS = ("title", "props")

    def get(self, id):
        return Part.query.get(id)

    def patch(self, id):
        part = Part.query.get(id)
        for k, v in self.get_args().items():
            setattr(part, k, v)
        self.update(part)
        return part

    def delete(self, id):
        part = Part.query.get(id)
        self.remove(part)
        return {}, 204


api.add_resource(PartResource, "/parts/<id>/")


class PartChildrenResource(BaseResource):
    ARGS = ("children",)

    def get(self, id):
        # List all children of a given part
        part = Part.query.get(id)
        return part.children

    def post(self, id):
        # Add parts to an assembly
        args = parser.parse_args()
        part = Part.query.get(id)
        if not part:
            abort(
                400, error_message=f"Part {id} does not exist"
            )
        for child_id in args["children"]:
            if child_id == id:
                abort(400, error_message=f"You cannot add an part as its own child")
            child = Part.query.get(child_id)
            part.children.append(child)
        self.create(part)
        return part.children

    def delete(self, id):
        # Remove parts from an assembly
        args = parser.parse_args()
        part = Part.query.get(id)
        if not part:
            abort(
                400, error_message=f"Part {id} does not exist"
            )
        for child_id in args["children"]:
            child = Part.query.get(child_id)
            if not child:
                abort(400, error_message=f"Part {child_id} does not exist")
            try:
                part.children.remove(child)
            except ValueError:
                abort(
                    400, error_message=f"Part {child_id} is not a child of part {id}"
                )
        self.create(part)
        return part.children


api.add_resource(PartChildrenResource, "/parts/<id>/children/")


class AssemblyListResource(BaseResource):

    def get(self):
        # List all assemblies
        return Part.get_assemblies()


api.add_resource(AssemblyListResource, "/assemblies/")


class TopAssemblyListResource(BaseResource):

    def get(self):
        # List all top-level assemblies
        return Part.get_top_assemblies()


api.add_resource(TopAssemblyListResource, "/top-assemblies/")


class SubAssemblyListResource(BaseResource):

    def get(self):
        # List all sub-assemblies
        return Part.get_sub_assemblies()


api.add_resource(SubAssemblyListResource, "/sub-assemblies/")


class ComponentListResource(BaseResource):

    def get(self):
        # List all components
        return Part.get_components()


api.add_resource(ComponentListResource, "/components/")


class OrphanListResource(BaseResource):

    def get(self):
        # List all orphan components
        return Part.get_orphans()


api.add_resource(OrphanListResource, "/orphans/")


class ParentListResource(BaseResource):

    def get(self, id):
        # List all direct and indirect parents of a component
        return Part.get_parents(id)


api.add_resource(ParentListResource, "/parents/<id>/")

if __name__ == "__main__":
    db.create_all()
    app.run(debug=os.environ.get("DEBUG", False))
